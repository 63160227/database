/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.databaseproject.dao.service;

import com.databaseproject.dao.UserDao;
import com.databaseproject.model.User;

/**
 *
 * @author acer
 */
public class UserService {
    public User login(String name, String password) {
        UserDao userDao = new UserDao();
        User user=  userDao.getByName(name);
        try{
        if(user!=null&&user.getPassword().equals(password)){
            return user;
        }
        }catch(Exception ex){
            System.out.println("error");
        }
        return null;
    }

}
