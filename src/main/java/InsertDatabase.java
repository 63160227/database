
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author acer
 */
public class InsertDatabase {
    public static void main(String[] args) {
         Connection conn = null;
        String url = "jdbc:sqlite:DCoffee.db";
        //Connection database
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been eatablish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        
        //Insert
        String sql = "INSERT INTO product(PD_NAME) VALUES (?);";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "Cookie");
            int status = stmt.executeUpdate();
            //ResultSet key = stmt.getGeneratedKeys();
            //key.next();
            //System.out.println(""+key.getInt(1));

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //Close Database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
