
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author acer
 */
public class DeleteDatabase {
     public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:DCoffee.db";
        //Connection database
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been eatablish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        
        //Delete
         String sql = "DELETE FROM product WHERE PD_ID = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 12);
            int status = stmt.executeUpdate();
            //ResultSet key = stmt.getGeneratedKeys();
            //key.next();
            //System.out.println(""+key.getInt(1));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }

        //Close Database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
